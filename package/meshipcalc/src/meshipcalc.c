#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

unsigned long i2l( char *ip )
{
	struct in_addr ip_temp;

	inet_aton( ip, &ip_temp );
	return( ip_temp.s_addr );
}

char *l2i( unsigned long ip )
{
	struct in_addr ip_temp;

	ip_temp.s_addr = ip;
	return( inet_ntoa( ip_temp ) );
}

int main (int argc, char *argv[])
{
	unsigned long ip_guifi,ip_temp, ip_dhcp;
	char buff[16];
	unsigned int rang, protocol, device, dhcp_rang;

	if ( argc < 3 ) {

		printf("Calculadora d'IPs de la mesh sobre la IP Pública.\n");
		printf("   Use: %s IP-guifi Rang_mesh(0-31) [Protocol] [Device|Dhcp]\n", argv[0] );
		printf("        IP-guifi   : La adreça IP publica de guifi.\n");
		printf("        Rang_mesh  : Un numero entre el 0-31 que és el vostre rang assignat.\n");
		printf("        Protocol   : 0-OLSR  1-BMX 2-BATMAN 3-DHCP.\n");
		printf("        Device|Dhcp: Si Protocol = 0|1|2 -> device 0-7 (Normalment eth0, eth1, ath0, ath1, étc).\n");
		printf("                     Si Protocol = 3 -> dchp 0-7 (On 0 => Start dhcp1, 1=> End dhcp1, 2 => Start dhcp2...)\n");
		return(-1);

	}
	protocol = 0;
	device = 0;

	if ( argc >= 4 ) protocol = atoi( argv[3] ); 
	if (argc >= 5 ) device = atoi( argv[4] ); 

	rang = atoi( argv[2] );
	ip_guifi = i2l ( argv[1] );

	if (ip_guifi == i2l("255.255.255.255")) {

		printf("IP incorrecte.\n");
		return(-2);

	}	
	if (rang > 31) {
		printf("Rang incorrecte.\n");
		return(-3);
	}
	if (protocol > 3) {
		printf("Protocol incorrecte.\n");
		return(-4);
	}

	if (device > 7) {
		printf("Device incorrecte.\n");
		return(-5);
	}
	

	ip_temp = ip_guifi & i2l("0.0.0.255");

	if (protocol == 3)
	{
		
		rang = (rang << 4);
		if ((device % 2) == 0) rang++;
		else rang += 15;
		
		dhcp_rang = (device / 2) + 28;

		sprintf (buff, "172.%d.0.%d", dhcp_rang, rang);
		if (i2l("0.0.0.255") == 255) {
			ip_temp = (ip_temp << 8) | i2l(buff);
		} else {
			ip_temp = (ip_temp >> 8) | i2l(buff);
		}

	}
	else 
	{
		if (device > 3) {
			rang += 128; // Activem el bit 1
			device -= 4;
		}
		protocol = (protocol * 4) + 16 + device;
		
		sprintf (buff, "172.%d.%d.0",protocol, rang ); 
		ip_temp |= i2l(buff);
		
	}
	
	printf("%s\n",l2i(ip_temp));

}
