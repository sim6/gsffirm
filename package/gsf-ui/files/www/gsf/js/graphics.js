var globalBMXData = [];
var globalDEVData = [];
var arrGraphs = [];
function inArray(arr, valor){
  for(var z=0; z < arr.length; z++){
    if(arr[z] == valor) return true;
  }
  return false;
}
// BMX
function getValues_bmx(){
  var active_ip = [];
  var o = this; //Seria el graphic
  newLinks = globalBMXData[o.id];
  // Pintem les linies.
  for (var k in newLinks){
    active_ip.push(k);
    var line_object = o.getLine(k); 
    if( line_object == null) {
       line_object = o.addLine(k,0);
       line_object.index = o.getIndex();
    }
    line_object.addValue(newLinks[k]);
  }
  // Borrem linies que ja no porten dades.
  for(var i=0; i < o.lines.length; i++){
    if(!inArray(active_ip, o.lines[i].label)) o.lines.splice(i,1);
  }
}
function interval_bmx(alt, ample, interval){
  $.ajax({
    url: "/cgi-bin/bmx-map-json?json",
    success: function(d) {
      arrDades = [];
      for(var ip in d){
        node = d[ip];
        // Existeix el graphic d'aquest node?
        links = node['links'];
        // Creem un llistat dels links del node quedant-nos amb la millor conexió.
        var newLinks = new Array;
        for (var k in links){
          ns = links[k];
          if (typeof(newLinks[ns['ip']]) != 'undefined') {
            if (newLinks[ns['ip']] < ns['rtq']) newLinks[ns['ip']] = ns['rtq'];
          } else {
            newLinks[ns['ip']] = ns['rtq'];
          }
        }
        globalBMXData[ip] = newLinks;
        /* Existeix graphic? */
        nom = new String(ip);
        nom = nom.replace(/\./gi,'_');
        nom = 'graph_'+nom;
        if ( $('#'+nom).length == 0 ) {
          $('#bmx_graphs').append("<div style='float:left'><label>"+ip+"</label><div id='"+nom+"' class='graph_bmx' style='width:"+ample+"px;height:"+alt+"px;'></div></div>");
          var gt = new GraphTime(ip,nom, 20,interval);
          gt.callback = 'getValues_bmx';
          gt.interval();
          arrGraphs.push(gt);
        }
      }
    },
    dataType: 'json'
  });
  // Ens cridem a nosaltres mateixos per recuperar + dades, si no estem en stopAll!!!
  if (!stopAll) setTimeout('interval_bmx('+alt+', '+ample+', '+interval+')',interval);
}
// DEVICES
function getValues_devices(){
  var o = this; //Seria el graphic
  dades = globalDEVData[o.id];
  // Pintem les linies.
  for (var k in dades){
    var line_object = o.getLine(k); 
    if( line_object == null) {
       line_object = o.addLine(k,0,"inc");
       line_object.index = o.getIndex();
    }
    line_object.addValue(dades[k]);
  }
}

function interval_devices(alt, ample, interval){
  $.ajax({
    url: "/cgi-bin/meshinfo001",
    success: function(d) {
      arrDades = [];
      for(var dv in d){
        globalDEVData[dv] = [];
        globalDEVData[dv]['RX'] = d[dv]['RX']/interval;
        globalDEVData[dv]['TX'] = d[dv]['TX']/interval;
        nom = new String(dv);
        nom = nom.replace(/\./gi,'_');
        nom = 'graph_'+nom;
        if ( $('#'+nom).length == 0 ) {
          $('#devices_graphs').append("<div style='float:left'><label>"+dv+"</label><div id='"+nom+"' class='graph_dev' style='width:"+ample+"px;height:"+alt+"px;'></div></div>");
          var gt = new GraphTime(dv,nom, 20,interval);
          gt.callback = 'getValues_devices';
          gt.interval();
          arrGraphs.push(gt);
        }
        
      }
    },
    dataType: 'json'
  });
  // Ens cridem a nosaltres mateixos per recuperar + dades, si no estem en stopAll!!!
  if (!stopAll) setTimeout('interval_devices('+alt+', '+ample+', '+interval+')',interval);
}

