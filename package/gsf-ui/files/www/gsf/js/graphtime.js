var stopAll = false;
function LineTime(label, color, tipus, dades) {
  this.start(label, color, tipus, dades);
}
LineTime.prototype.start = function(label, color, tipus, dades) {
  this.dates = [];
  this.dates_inc = [];
  this.tipus = "normal";
  this.label = label;
  this.color = 0;
  this.index = 0;
  this.page;
  this.callback=null;
  this.maxval;

  if( typeof(color) != "undefined" ) this.color = color;
  if( typeof(tipus) != "undefined" ) this.tipus = tipus;
  if( typeof(dades) != "undefined" ) this.dates = dades;

  return this;
}
LineTime.prototype.addValue = function(newval) { 
  if (this.tipus == "inc"){ 
    if (this.dates.length > 0) {
      lastval = this.dates_inc[this.dates_inc.length-1];
      this.dates_inc.push(newval);
      if (this.dates_inc.length == this.maxval) this.dates_inc.shift();  
      newval = newval - lastval;
    } else {
      this.dates_inc.push(newval);
      newval = 0;
    }
  }
  this.dates.push([this.index++,newval]);
  if (this.dates.length == this.maxval) this.dates.shift();  
}
LineTime.prototype.getNewValue = function() {
  var obj = this;

  $.ajax({
    url: page,
    success: function(data) {
      obj.addValue(parseFloat(data));
    },
    dataType: "text"
  });
}

function GraphTime(id, d, m, i) {
  this.start(id, d, m, i);
}
GraphTime.prototype.start = function(id, div, maxval, interval) {
  this.id = id;
  this.maxval = maxval;
  this.divhook = div;
  this.stop = false;
  this.intervalTimeout = interval;
  this.callback = null;
  this.lines = new Array();

}
GraphTime.prototype.draw = function(){
  var arrTemp = [];
  var index = 0;
  for (var l in this.lines) {
    var obj = this.lines[l];
    var c = (obj.color == 0) ? index++ : obj.color;
    arrTemp.push({color: c, label: obj.label, data: obj.dates, lines: { show: true, fill: 0.3 },points: { show: true }})
  }
  $.plot($("#"+this.divhook), arrTemp);  
}
GraphTime.prototype.addLine = function(label, color, tipus, dades) {
  var lt = new LineTime(label,color, tipus, dades);
  lt.maxval = this.maxval;
  this.lines.push(lt);
  return lt; 
}
GraphTime.prototype.doStop = function () {
  this.stop = true;
}
GraphTime.prototype.getStart = function () {
  this.stop = false;
  this.interval();
}
GraphTime.prototype.interval = function() {
  if ((!this.stop) && (!stopAll)) {
    var obj = this;
    /* Callback del grafic */
    if(obj.callback != null) {
      eval(obj.callback+".call(obj);");
    } 
    /* Callbacks de les linies */
    for(var l in obj.lines){
      var lt_obj = obj.lines[l];
      if(lt_obj.callback != null) {
        eval(lt_obj.callback+".call(lt_obj);");
      } 
    }
    this.draw();
    setTimeout(function() {obj.interval()} ,obj.intervalTimeout);
  }
}
GraphTime.prototype.getLine = function(ip) {
  for(var l in this.lines){
    if (this.lines[l].label == ip) return this.lines[l];
  }
  return null;
}
GraphTime.prototype.getIndex = function() {
  for(var l in this.lines){
    return this.lines[l].index - 1;
  }
  return 0;
}
function getValues_devices(){
  var o = this;
  $.ajax({
    url: o.page,
    success: function(d) {
      o.lines[0].addValue(d.data[0]);
      o.lines[1].addValue(d.data[1]);
    },
    dataType: 'json'
  });
}
