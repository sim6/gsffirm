   function $(id){
    return document.getElementById(id);
   }
   function noError(id){
    var item = $(id);
    item.style.color="";
    item.style.backgroundColor="";
    if(null!=$(id+"Error")){
     $(id+"Error").parentNode.removeChild($(id+"Error"));
    }
    return true;
   }
   function doError(id,msg,cond){
    var item = $(id);
    if (cond) {
     item.style.color="red";
     item.style.backgroundColor="#F5FFD3";
     if(null==$(id+"Error")){
      nouP = document.createElement("p");
      nouP.id=id+"Error";
      nouP.className="error";
      nou = document.createTextNode(msg);
      nouP.appendChild(nou);
      item.parentNode.appendChild(nouP);
     }
     return false;
    } else {
     return noError(id);
    }
   }
   function checkIP(id){
    var item = $(id);
    var filter = /^\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b$/;
    var cond = !filter.test(item.value);
    return doError(id,_JSSTR_iperror_,cond);
   }
   function checkFreenetworksIP(id){
    var item = $(id);
    if(checkIP(id)){
     var filter = /^\b(10)\..*$/;
     var cond = !filter.test(item.value);
     return doError(id,_JSSTR_ipfreenetworkserror_,cond);
    }else{
     return false;
    }
   }
   function checkLANIP(id){
    var item = $(id);
    if(checkIP(id)){
     var filter = /^\b(192)\.(168)\..*$/;
     var cond = !filter.test(item.value);
     return doError(id,_JSSTR_ipprivateerror_,cond);
    }else{
     return false;
    }
   }
   function checkNum(id){ 
    var num = $(id).value;
    var cond = isNaN(num);
    return doError(id,_JSSTR_numbererror_,cond);
   }
   function checkEmail(id){
    var item = $(id);
    var filter = /^[a-z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+(\.[a-z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+)*(@|-at-)[a-z0-9-]+(\.[a-z0-9-]+)*\.([a-z]{2,})$/;
    var cond = !filter.test(item.value);
    return doError(id,_JSSTR_emailerror_,cond);
   }
   function checkGeneral(){
    var cip = checkFreenetworksIP("ip");
    var clon = checkNum("lon");
    var clat = checkNum("lat");
    var cemail = checkEmail("email");
    return cip && clon && clat && cemail;
   }
   function checkDns(){ 
    var cdns0 = checkIP("dns0");
    var cdns1 = checkIP("dns1");
    var cdns2 = checkIP("dns2");
    return cdns0 && cdns1 && cdns2;
   }
   function checkLan(){ 
    if($("nodhcp").checked==true||($("dhcpd").checked==true&&$("inet").checked==true)){
     var ciplan = checkLANIP("iplan");
     var cmasklan = checkIP("masklan");
     cdhcp = ciplan && cmasklan;
    }else{
     cdhcp = true;
    }
    if($("inet").checked==true&&$("dhcpc").checked==false){
     cinet = checkLANIP("gw");
    }else{
     cinet = true;
    }
    return cdhcp && cinet;
   }
   function checkServer(){ 
    if($("server").checked==true){
     var cipserver = checkFreenetworksIP("ipserver");
     var cipserverlan = checkIP("ipserverlan");
     cserver = cipserver && cipserverlan;
    }else{
     cserver = true;
    }
    return cserver;
   }
   function checkWireless(){
    //ToDo
   }
   function change(id){
    var item = $(id);
    switch (id){
     case "inet":
      if(item.checked==true){
       $("bw").disabled=false;
       $("bwuser").disabled=false;
       $("bwguest").disabled=false;
       if($("bw").value==""){
        $("bw").value=BW_DEF;
       }
       if($("bwuser").value==""){
        $("bwuser").value=BW_DEF_USER;
       }
       if($("bwguest").value==""){
        $("bwguest").value=BW_DEF_GUEST;
       }
       $("gwtest").disabled=false;
       $("splash_delay").disabled=false;
       $("splash_delay_guests").disabled=false;
       $("splash_message").disabled=false;
       $("splash_whitelist_domain").disabled=false;
       $("splash_whitelist_client").disabled=false;
       if($("dhcpc").checked==false){
        $("nat").disabled=false;
        $("iplan").disabled=false;
        $("masklan").disabled=false;
        $("gw").disabled=false;
       }
      }else{
       $("bw").disabled=true;
       $("bwuser").disabled=true;
       $("bwguest").disabled=true;
       $("gwtest").disabled=true;
       $("splash_delay").disabled=true;
       $("splash_delay_guests").disabled=true;
       $("splash_message").disabled=true;
       $("splash_whitelist_domain").disabled=true;
       $("splash_whitelist_client").disabled=true;
       if($("dhcpc").checked==false){
        $("nat").disabled=true;
        $("iplan").disabled=true;
        noError("iplan");
        $("masklan").disabled=true;
        noError("masklan");
        $("gw").disabled=true;
        noError("gw");
       }
      }
     break;
     case "dhcpd": 
      if($("inet").checked==true){
       $("nat").disabled=false;
       $("iplan").disabled=false;
       $("masklan").disabled=false;
       $("gw").disabled=false;
      }else{
       $("nat").disabled=true;
       $("iplan").disabled=true;
       noError("iplan");
       $("masklan").disabled=true;
       noError("masklan");
       $("gw").disabled=true;
       noError("gw");
      }
     break;
     case "dhcpc":
      $("nat").disabled=false;
      $("iplan").disabled=true;
      noError("iplan");
      $("masklan").disabled=true;
      noError("masklan");
      $("gw").disabled=true;
      noError("gw");
     break
     case "nodhcp":
      $("nat").disabled=false;
      $("iplan").disabled=false;
      $("masklan").disabled=false;
      if($("inet").checked==true){
       $("gw").disabled=false;
      }
     break;
     case "server":
      if(item.checked==true){
       $("ipserver").disabled=false;
       $("ipserverlan").disabled=false;
      }else{
       $("ipserver").disabled=true;
       noError("ipserver");
       $("ipserverlan").disabled=true;
       noError("ipserverlan");
      }
    }
   }

