/*

 BMX map

 Copyright (C) 2008-2011 Agustí Moll i Garcia, Simó Albert i Beltran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU AfferoGeneral Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Version 0.2.5.20110525.3.0

*/

var bmxMap = new Array;

//Global variables for getBmxMapURL function.
bmxMap['SCRIPT_LOCATION'] = "js/make_map.js";

setBmxMapURL();

/**
 * Generate the bmx-map files URL and store this on bmxMapURL global variable.
 * This function uses two global variables:
 *	bmxMap['SCRIPT_LOCATION']: The global constant with string of the relative path of this file.
 *	bmxMap['url']: The global variable to save URL of bmx-map files.
 */

function setBmxMapURL()
{
	if(!bmxMap['url'])
	{
		var filter = new RegExp(".*"+bmxMap['SCRIPT_LOCATION']);
		
		// static include
		var scripts = document.getElementsByTagName("script");

		for(var script in scripts)
		{
			if(filter.test(scripts[script].src))
			{
				bmxMap['url'] = scripts[script].src.replace(bmxMap['SCRIPT_LOCATION'],"");
				return;
			}
		}
		// jQuery ajax $.getScript
		if($ != undefined)
		{
			$(document).ajaxSuccess(function(e, xhr, s){
				if(filter.test(s.url))
				{
					filter = new RegExp(bmxMap['SCRIPT_LOCATION']+".*");
					bmxMap['url'] = s.url.replace(filter,"");
				}
        	});
		}
	}
}

/**
 * Get the bmx-map files URL.
 * This function uses the next global variable:
 *	bmxMap['url']: The global variable to save URL of bmx-map files.
 * @return {String} The bmx-map files URL.
 */

function getBmxMapURL()
{
	if(bmxMap['url'])
	{
		return bmxMap['url'];
	}
}

// Function to create the map
function map(type){
	OpenLayers.IMAGE_RELOAD_ATTEMPTS = 3;
	OpenLayers.Util.onImageLoadErrorColor = "transparent";

	var lat;
	var lon;
	var isOriginal = true;
	var zoom=15;
	var options = {
        projection: new OpenLayers.Projection("EPSG:900913"),
        displayProjection: new OpenLayers.Projection("EPSG:4326"),
        units: "m",
        numZoomLevels: 18,
        maxResolution: 156543.0339,
        maxExtent: new OpenLayers.Bounds(-20037508, -20037508,
                                         20037508, 20037508.34),
			controls:[
					new OpenLayers.Control.Navigation(),
					new OpenLayers.Control.PanZoomBar(),
                    new OpenLayers.Control.ScaleLine(),
                    new OpenLayers.Control.MousePosition(),
					new OpenLayers.Control.LayerSwitcher()]
 		};
    bmxMap['map'] = new OpenLayers.Map('map', options);

    var osmLayer = new OpenLayers.Layer.OSM();

    var layer_style = OpenLayers.Util.extend({}, OpenLayers.Feature.Vector.style['default']);
    layer_style.fillOpacity = 0.2;
    layer_style.graphicOpacity = 1;

	var style_green = OpenLayers.Util.extend({}, layer_style);
    style_green.strokeColor = "green";
    style_green.fillColor = "green";
    style_green.strokeWidth = 3;

	var style_orange = OpenLayers.Util.extend({}, layer_style);
    style_orange.strokeColor = "orange";
    style_orange.fillColor = "orange";
    style_orange.strokeWidth = 2;

	var style_red = OpenLayers.Util.extend({}, layer_style);
    style_red.strokeColor = "red";
    style_red.fillColor = "red";

	var meshGoodLinks = new OpenLayers.Layer.Vector("Mesh Good Links", {style: style_green});
	var meshNormalLinks = new OpenLayers.Layer.Vector("Mesh Normal Links", {style: style_orange});
	var meshBadLinks = new OpenLayers.Layer.Vector("Mesh Bad Links", {style: style_red});

	var meshNodes = new OpenLayers.Layer.Markers( "Mesh Nodes" );


	// Draw nodes.
	for (var k in node){
		var node_actual = node[k];
		if(isOriginal) {lon = node_actual['long']; lat = node_actual['lat']; isOriginal = false;}
		drawGraphicNode(k, node_actual, meshNodes);
	}

	for (var k in node) {
		var node_actual = node[k];
		for(var l in node_actual['links']) {
			var node_link_actual = node_actual['links'][l];
			var nla_ip = node_link_actual['ip'];
			if(node[nla_ip]){
				var node_actual_link = node[nla_ip];
				drawLine(node_link_actual,node_actual['long'],node_actual['lat'],
						node_actual_link['long'],node_actual_link['lat'],
						meshGoodLinks, meshNormalLinks, meshBadLinks);
			}
		}
	}

	// Layer of the pointer.
	var pointer = new OpenLayers.Layer.Vector("Pointer");


	// Hide the layer of the nodes if map type is pointer,
	// and hide the layer of the pointer if map type isn't pointer.
	if ( type == 'pointer' ) {
		meshNodes.visibility = false;
	}
	else {
		pointer.visibility = false;
	}

	// Create image of pointer and get latitude and longitude if layer of the pointer is clicked.
	var map = bmxMap['map'];
	map.events.register('click', map, handleMapClick);
	function handleMapClick(e){
		var lonlat = bmxMap['map'].getLonLatFromViewPortPx(e.xy);
		var box = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(lonlat.lon,lonlat.lat));
		pointer.destroyFeatures();
		pointer.addFeatures(box);
		lonlat.transform(bmxMap['map'].getProjectionObject(), bmxMap['map'].displayProjection);
		if($('lat')) $('lat').value=lonlat.lat;
		if($('lon')) $('lon').value=lonlat.lon;
	}


	// Add layers to the map and center the map.
	bmxMap['map'].addLayers([osmLayer, meshBadLinks, meshNormalLinks, meshGoodLinks, meshNodes, pointer]);
	bmxMap['map'].setCenter(new OpenLayers.LonLat(lon, lat).transform(new OpenLayers.Projection("EPSG:4326"), new
	     OpenLayers.Projection("EPSG:900913")), zoom);
}
function get_info_node(ip_node){
	var eDIV = document.createElement("div");
	var n = node[ip_node];
	var eLink = document.createElement("a");
	eLink.setAttribute("href","http://"+ip_node+"/map/");
	var eH = document.createElement("h3");
	eLink.appendChild(document.createTextNode(ip_node+" "+n['name']));
	eH.appendChild(eLink);
	eDIV.appendChild(eH);
	
	var email = n['email'];
	var eMLink = document.createElement("a");
	eMLink.setAttribute("href","mailto:"+email);
	eMLink.appendChild(document.createTextNode(email));
	var eM = document.createElement("p");
	eM.appendChild(eMLink);
	eDIV.appendChild(eM);

	for(var l in n['links']) {
		var neigh = n['links'][l];
		if (neigh['ip']) {
			neigh_ip = neigh['ip'];
			nla = n['links'][l];
			nla_info = node[neigh_ip];
			var eNeg = document.createElement("b");
			eNeg.appendChild(document.createTextNode(neigh['ip']));
			eDIV.appendChild(eNeg);
			eDIV.appendChild(document.createTextNode(" ("+nla['dev']+"): "+nla['rtq']+"("+nla['rq']+"/"+nla['tq']+")"));		
			var eBR = document.createElement("br");
			eDIV.appendChild(eBR);
		}
	}
	return eDIV;
}

// Function to select icon for a node.
function escullirImatge(hw){
	var url = getBmxMapURL();
	var imatge = new Array();
	var hw_str = new String(hw);
	switch (hw_str.toLowerCase())
	{
		case 'alix':
		case 'alix2c2':
		case 'alix2d2':
			imatge = {'trasto_img' : url+'img/alix_trasto.png' , 'trasto_x' : -10 , 'trasto_y' : -50, 'trasto_width' : 32, 'trasto_height': 50 };
			break;
		case 'linksys':
		case 'wrt54g':
			imatge = {'trasto_img': url+'img/wrt54g.png', 'trasto_x': -11, 'trasto_y': -24, 'trasto_width': 22, 'trasto_height': 24};
			break;
		case 'ns':
		case 'nanostation':
			imatge = {'trasto_img': url+'img/ns_trasto.png', 'trasto_x': -8, 'trasto_y': -50, 'trasto_width': 15, 'trasto_height': 50 };
			break;
		default:
			imatge = {'trasto_img' : url+'img/thumbtack.png' , 'trasto_x' : -10 , 'trasto_y' : -35, 'trasto_width' : 30, 'trasto_height': 37 };
			break;
	}
	return(imatge);
}

// Function to draw nodes.
function drawGraphicNode(ip, node_actual, lyr){
	var ima = escullirImatge(node_actual['hw']);
	var size = new OpenLayers.Size(ima['trasto_width'],ima['trasto_height']);
	var offset = new OpenLayers.Pixel(ima['trasto_x'], ima['trasto_y']);
	var icon = new OpenLayers.Icon(ima['trasto_img'],size,offset);
	var pos = new OpenLayers.LonLat(node_actual['long'], node_actual['lat']).transform(new OpenLayers.Projection("EPSG:4326"), new OpenLayers.Projection("EPSG:900913"));
	var marca = new OpenLayers.Marker(pos,icon);

	marca.events.register(
		'mousedown',
		this,
		function(e){
			if (this.popup == null)
			{
				this.popup = new OpenLayers.Popup.FramedCloud(
						ip,
						pos,
						null,
						get_info_node(ip).innerHTML,
						null,
						true
					)
				bmxMap['map'].addPopup(this.popup);
			}
			else
			{
				this.popup.destroy();
				delete this.popup;
			}
		}
	);

	lyr.addMarker(marca);
	}
function drawLine(info,x1,y1,x2,y2,lyr_g, lyr_o, lyr_r){
	var arr = [];
	arr.push(new OpenLayers.Geometry.Point(x1, y1).transform(new OpenLayers.Projection("EPSG:4326"), new
	     OpenLayers.Projection("EPSG:900913")));
	arr.push(new OpenLayers.Geometry.Point(x2, y2).transform(new OpenLayers.Projection("EPSG:4326"), new
		 OpenLayers.Projection("EPSG:900913")));
	var ln = new OpenLayers.Geometry.LineString(arr);
    
	var lineaFeature = new OpenLayers.Feature.Vector(ln);
	if (info['pq'] > 80) lyr = lyr_g;
	else lyr = lyr_o;
	if (info['pq'] < 20) lyr = lyr_r;
	lyr.addFeatures(lineaFeature);
}
