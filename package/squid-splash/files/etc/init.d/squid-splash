#!/bin/sh /etc/rc.common
#
# Squid init script
#
# Copyright (C) 2008 graciasensefils.net - Simó Albert
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Version 0.2.2.2010.4.11.1159
# Usage: $0 start|stop

START=98
STOP=01

SQUID_CONFIG=/etc/squid/squid.conf

iptables_rules() {
 case $1 in
  flush)
   iptables -t nat -D PREROUTING -j squid-splash
   iptables -t nat -F squid-splash
   iptables -t nat -X squid-splash
  ;;
  *)	
   iptables -t nat -N squid-splash
   iptables -t nat -A squid-splash -d 10.0.0.0/8 -j RETURN
   iptables -t nat -A squid-splash -d 172.16.0.0/12 -j RETURN
   iptables -t nat -A squid-splash -d 192.168.0.0/16 -j RETURN
   for i in bat0 gate0 ath0 ath1
   do
    iptables -t nat -A squid-splash -i $i -p tcp --dport 80 -j REDIRECT --to-port 3128
   done
   iptables -t nat -A squid-splash -j RETURN
   iptables -t nat -A PREROUTING -j squid-splash
  ;;
 esac
}

squid_set_config(){

	bw_user="$(uci -q get gsf.gateway.bandwidth_per_user)"

	if [ -n "$bw_user" ] && ! grep -q -E "^\s*delay_parameters 1 ${bw_user}00/${bw_user}00$" $SQUID_CONFIG
	then
		sed -i -e "s:^\(\s*delay_parameters 1 \).*/.*$:\1${bw_user}00/${bw_user}00:g" $SQUID_CONFIG
	fi

	bw_guest="$(uci -q get gsf.gateway.bandwidth_per_guest)"

	if [ -n "$bw_guest" ] && ! grep -q -E "^\s*delay_parameters 2 ${bw_guest}00/${bw_guest}00$" $SQUID_CONFIG
	then
		sed -i -e "s:^\(\s*delay_parameters 2 \).*/.*$:\1${bw_guest}00/${bw_guest}00:g" $SQUID_CONFIG
	fi

}

start() {
 if [ "$(uci get gsf.gateway)" = "on" ]
 then
  if [ "$1" != "soft" ]
  then
   squid_set_config
   [ ! -d /var/cache ] && mkdir /var/cache
   [ ! -d /var/logs ] && mkdir /var/logs
   chmod a+w /var/cache /var/logs
   squid -z
   squid
   echo -n "Please wait. "
   while ! ps aux | grep -q "squid) $"
   do
    echo -n .
    sleep 1
   done
   echo "Squid started."
  fi
  # If squid are running create iptables rules.
  if ps aux | grep -q "squid) $"
  then
    iptables_rules
    echo "Squid iptables rules added."
  fi
 fi
}

stop() {
 iptables_rules flush
 echo "Squid iptables rules deleted."
 case "$1" in
   soft)
   ;;
   correct)
     squid -k shutdown
     echo -n "Please wait. "
     while ps aux | grep -q "squid) $"
     do
       echo -n .
       sleep 1
     done
     echo " Thanks."
   ;;
   *)
     killall -9 squid
     rm -rf /var/cache
   ;;
 esac
}
