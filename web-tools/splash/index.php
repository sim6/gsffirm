<?php
/*
# GSF PHP Splash Web Page (captive portal without autentication used with squid-splash)
#
# Copyright (C) 2009 graciasensefils.net - Simó Albert
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Version 0.2.4.2010.9.9.1814
*/
 $EMPTY_URL = "http://graciasensefils.net";
 $EMPTY_EMAIL = "info@graciasensefils.net";
 $message = stripslashes($_GET["msg"]);
 if($_GET[url] == ""){
  $url = $EMPTY_URL;
 }else{
  $url = $_GET[url];
 }
 if($_GET[email] == ""){
  $email = $EMPTY_EMAIL;
 }else{
  $email = $_GET[email];
 }
 /*Select the language the visitor has selected, or that has been drawn from his browser language */
 $language = $_GET[lang];
 if($_GET[lang] == ""){
  $language = substr($_SERVER["HTTP_ACCEPT_LANGUAGE"],0,2);
 }
 switch ($language){
  //CATALAN
  case 'ca':
   $title = 'Benvinguda a graciasensefils.net - guifi.net';
   $welcome = 'Benvinguda';
   $splash = '<p>Connectar-se a aquesta xarxa implica acceptar els termes i condicions de la <a href="http://guifi.net/ComunsSensefils">Llicència Comuns Sensefils</a></p><p>Si us plau, col.labori en l\'expansió d\'aquesta xarxa.</p><p>Si tens qualsevol dubte pregunta a <a href="mailto:'.$email.'">'.$email.'</a>.</p><p>Pots gaudir dels <a href="http://graciasensefils.net/doku.php#serveis">serveis disponibles</a>.</p>';
   $submit = 'Continuar navegant';
   $powered = 'Powered by <a href="http://graciasensefils.net">graciasensefils.net</a>';
  break;
  //ENGLISH
  case 'en':
  default:
   $title = 'Welcome to graciasensefils.net - guifi.net';
   $welcome = 'Welcome';
   $splash = '<p>Connect to this network means accept the terms and conditions of the <a href="http://guifi.net/WCL_EN">Common Wireless License</a></p><p>Please, collaborate in this network expansion.</p><p>If you have any questions ask <a href="mailto:'.$email.'">'.$email.'</a>.</p><p>You can enjoy the <a href="http://graciasensefils.net/doku.php#serveis">available services</a>.</p>';
   $submit = 'Continue browsing';
   $powered = 'Powered by <a href="http://graciasensefils.net">graciasensefils.net</a>';
  break;
 } 
 header('Access-Control-Allow-Origin: *');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
 <head>
  <meta http-equiv="Pragma" content="no-cache">
  <meta http-equiv="Expires" content="-1">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title><?php echo $title; ?></title>
  <style>
   body { 
    text-align: center;
    margin: 0;
    border: 3em solid lightgrey;
    padding: 2em 5em;
    border-radius: 25%;
    -moz-border-radius: 25%;
    -webkit-border-radius: 25%;
   }
   img {
    border:0;
   }
   ul {
    text-align: left;
   }
   #submit {
    font-size: x-large;
   }
   #powered {
    font-size: x-small;
   }
  </style>
  <script src="http://jqueryjs.googlecode.com/files/jquery-1.3.2.min.js" type="text/javascript"></script>
  <script type="text/javascript">
$(document).ready(function(){
 $("#externaldata").append('<div id="map"></div>');
 $("#map").load("http://gsf.guifi.net/gsf/map");
});
  </script>
 </head>
 <body>
  <h1><?php echo $welcome; ?></h1>
  <div>
<?php
if (preg_match('/10\.139\.173\..*/', $_GET[proxy])) {
?>
   <a href="http://poblenousensefils.net">
    <img src="http://poblenousensefils.net/imatges/logo_P9SF.png" alt="poblenousensefils.net" />
   </a>
<?php
} elseif (preg_match('/10\.139\.81\..*/', $_GET[proxy])) {
?>
   <h2>Xarxa Oberta de Sant Joan Despí</h2>
<?php
} else {
?>
   <a href="http://graciasensefils.net">
    <img src="http://graciasensefils.net/graciasensefils.png" alt="GràciaSensefils.net" />
   </a>
<?php
}
?>
   <a href="http://guifi.net">
    <img src="http://graciasensefils.net/guifi.png" alt="guifi.net" />
   </a>
   <div id="powered">
<?php
if (!preg_match('/10\.139\.68\..*/', $_GET[proxy])) {
	echo $powered;
}
?>
   </div>
  </div>
  <div><?php echo $splash ?></div>
  <div id="proxydata">
   <?php echo $message; ?>
  </div>
  <p><a id="submit" href="<?php echo $url; ?>"><?php echo $submit; ?></a></p>
  <div id="externaldata">
  </div>
 </body>
</html>
